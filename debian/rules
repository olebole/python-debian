#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

# tell pybuild which module to install into the packages
export PYBUILD_NAME=debian

%:
	dh $@ --with python2,python3 --buildsystem pybuild

%.py: %.py.in debian/changelog
	sed -e 's/__CHANGELOG_VERSION__/$(DEB_VERSION)/' < $< > $@

override_dh_auto_clean: setup.py
	dh_auto_clean

override_dh_auto_build: setup.py
	dh_auto_build
	lib/debian/doc-debtags > README.debtags

override_dh_auto_test:
	# don't run the tests if suppressed with DEB_BUILD_OPTIONS=nocheck
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# run all the tests with both a UTF-8 aware locale and a non-UTF-8
	# aware locale to catch errors related to encoding.
	LC_ALL=C python -m unittest discover --verbose lib/
	LC_ALL=C.UTF-8 python -m unittest discover --verbose lib/
	LC_ALL=C python3 -m unittest discover --verbose lib/
	LC_ALL=C.UTF-8 python3 -m unittest discover --verbose lib/
endif

override_dh_auto_install:
	dh_auto_install
	# setuputils may produce different files for different versions of
	# python and dh_python3 will leave them behind.
	rm -rf $(CURDIR)/*/usr/lib/python3.*

override_dh_installexamples:
	# TODO: Enrico would like to keep wxssearch out of the package
	# but so far, he has no better place for it
	dh_installexamples -X wxssearch

dist: setup.py
	rm -f dist/python?debian-$(DEB_VERSION)*
	python3 setup.py sdist bdist_wheel bdist_egg
	python2 setup.py bdist_wheel bdist_egg
	echo "You can now: twine upload --sign dist/python?debian-$(DEB_VERSION)*"

.PHONY: dist
